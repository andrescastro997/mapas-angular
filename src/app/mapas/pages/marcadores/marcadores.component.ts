import { Component, ElementRef, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

interface MarcadorColor {
  color: string;
  marker?: mapboxgl.Marker;
  centro?: [number, number];
}
@Component({
  selector: 'app-marcadores',
  templateUrl: './marcadores.component.html',
  styles: [
    `
    .mapa-container{
      height: 100%;
      width: 100%;
    }

    .list-group {
      position: fixed;
      top: 20px;
      right: 20px;
      z-index: 99;

    }

    li{
      cursor: pointer
    }
    `
  ]
})
export class MarcadoresComponent implements OnInit, AfterViewInit {
  @ViewChild('mapa') divMapa!: ElementRef;
  mapa!: mapboxgl.Map;
  zoomLevel: number = 15;
  center: [number, number] =  [-107.54889404051724, 24.74739484529349 ];
  marcadores: MarcadorColor[] = [];
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    console.log('afterviewinit',this.divMapa);
    this.mapa = new mapboxgl.Map({
      container: this.divMapa.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.center,
      zoom: this.zoomLevel
    });
    this.leerLS();
    // const markerHtml: HTMLElement = document.createElement('div');
    // markerHtml.innerHTML = 'hola puto';
    // new mapboxgl.Marker(
    //   {
    //     element: markerHtml
    //   }
    // )
    //   .setLngLat(this.center)
    //   .addTo(this.mapa);
  }

  irAMarcador(marcador: mapboxgl.Marker){
    console.log(marcador);
    this.mapa.flyTo({
      center: marcador.getLngLat()
    });
  }

  agregarMarcador(){
    const color = "#xxxxxx".replace(/x/g, y=>(Math.random()*16|0).toString(16));
    console.log(color);
    const nuevoMarcador = new mapboxgl.Marker(
      {
        draggable: true,
        color: color
      }
    )
      .setLngLat(this.center)
      .addTo(this.mapa);
      this.marcadores.push({
        color: color,
        marker: nuevoMarcador
      });
      this.guardarMarcadoresLS();
      nuevoMarcador.on('dragend', ()=>{
        this.guardarMarcadoresLS();
      });
  }

  guardarMarcadoresLS(){
    const lngLatArr: MarcadorColor[] = [];
    this.marcadores.forEach(m =>{
      const color = m.color;
      const {lng, lat} = m.marker!.getLngLat();

      lngLatArr.push({
        color,
        centro: [lng, lat]
      });

      localStorage.setItem('marcadores', JSON.stringify(lngLatArr));

    })
  }

  leerLS(){
    if(!localStorage.getItem('marcadores')){
      return;
    }
    const lngLatArr: MarcadorColor[] = JSON.parse(localStorage.getItem('marcadores')!);
    console.log(lngLatArr);
    lngLatArr.forEach( m =>{
      const newMarker = new mapboxgl.Marker({
        color: m.color,
        draggable: true
      })
      .setLngLat(m.centro!)
      .addTo(this.mapa);

      this.marcadores.push(
        {
          marker: newMarker,
          color: m.color
        }
      )

      newMarker.on('dragend', ()=>{
        this.guardarMarcadoresLS();
      });
    });
  }
  borrarMarcador(i: number){
    console.log('borrar marcador');
    this.marcadores[i].marker?.remove();
    this.marcadores.splice(i, 1);
    this.guardarMarcadoresLS();
  }

}
