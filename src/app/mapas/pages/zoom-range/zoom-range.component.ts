import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-zoom-range',
  templateUrl: './zoom-range.component.html',
  styles: [
    `
    .mapa-container{
      height: 100%;
      width: 100%;
    }
    .row{
      background-color:white;
      z-index:999;
      position: fixed;
      bottom:50px;
      left: 50px;
      padding: 10px;
      border-radius: 5px;
      width: 400px;
    }
  `
  ]
})
export class ZoomRangeComponent implements AfterViewInit, OnDestroy {
  @ViewChild('mapa') divMapa!: ElementRef;
  mapa!: mapboxgl.Map;
  zoomLevel: number = 15;
  center: [number, number] =  [-107.54889404051724, 24.74739484529349 ]
  constructor() { 
  }

  ngAfterViewInit(): void {
    console.log('afterviewinit',this.divMapa);
    this.mapa = new mapboxgl.Map({
      container: this.divMapa.nativeElement,
      style: 'mapbox://styles/mapbox/streets-v11',
      center: this.center,
      zoom: this.zoomLevel
    });

    this.mapa.on('zoom', (e) =>{
     
      const zoomActual = this.mapa.getZoom();
      this.zoomLevel = this.mapa.getZoom();
    });

    this.mapa.on('zoomend', (e) =>{
      if(this.mapa.getZoom() > 18) {
        this.mapa.zoomTo(18);
      }
    });

    this.mapa.on('move', (e) =>{
      const target = e.target;
      const  {lng, lat} = target.getCenter();
      this.center = [lng, lat]
    })
  }

  ngOnDestroy(){
    this.mapa.off('zoom', () =>{});
    this.mapa.off('zoomend', () =>{});
    this.mapa.off('move', () =>{});
    
  }

  zoomOut(){
    console.log('zoom out');
    this.mapa.zoomOut();
  
  }

  zoomIn(){
    console.log('zoom in');
    this.mapa.zoomIn();
   
  }

  zoomCambio(valor: string){
    console.log(valor);
    this.mapa.zoomTo(Number(valor));
  }

}
